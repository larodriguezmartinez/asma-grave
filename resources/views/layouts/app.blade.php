<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fonts/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}"" rel="stylesheet">
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    @toastr_js
    @toastr_css
</head>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="{{ asset('img/profile_small.png') }}"/>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                                <span class="text-muted text-xs block">{{ Auth::user()->hospital }} <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li>
                                    <a class="dropdown-item" href="{{ route('perfil.show' , Auth::user()->id ) }}"><i class="fa fa-user" aria-hidden="true"></i> {{ __('Perfil') }} </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i> {{ __('Logout') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            AG
                        </div>
                    </li>
                    @auth
                        <li class="@if (Request::is('back-office'))  {{ 'active' }} @endif ">
                            <a href="{{ url('/back-office/') }}"><i class="fa fa-home" aria-hidden="true"></i> <span class="nav-label">{{ config('app.name') }}</span></a>
                        <li>
                        @if ( Auth::user()->role->id == 1 )
                        <li class="@if (Request::is('*/panel-control'))  {{ 'active' }} @endif ">
                                <a href="{{ route('panel-control') }}"><i class="fa fa-tachometer" aria-hidden="true"></i> <span class="nav-label">{{ __('Panel') }}</span></a>
                            </li>
                            <li class="@if (Request::is('*/gestion-usuarios'))  {{ 'active' }} @endif ">
                                <a href="{{ route('gestion-usuarios') }}"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">{{ __('Gestión Usuarios') }}</span></a>
                            </li>
                        @endif
                        <li class="@if (Request::is('*/registros'))  {{ 'active' }} @endif ">
                            <a href="{{ route('registros') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="nav-label">{{ __('Registros') }}</span></a>
                        </li>
                        <li class="@if (Request::is('*/visitas'))  {{ 'active' }} @endif ">
                            <a href="{{ route('visitas') }}"><i class="fa fa-eye" aria-hidden="true"></i> <span class="nav-label">{{ __('Visitas') }}</span></a>
                        </li>
                        <li class="@if (Request::is('*/transferencias'))  {{ 'active' }} @endif ">
                            <a href="{{ route('transferencias') }}"><i class="fa fa-exchange" aria-hidden="true"></i> <span class="nav-label">{{ __('Transferencia') }}</span></a>
                        </li>
                        <li class="@if (Request::is('*/perfil/*'))  {{ 'active' }} @endif ">
                            <a href="{{ route('perfil.show' , Auth::user()->id ) }}"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label">{{ __('Perfil') }}</span></a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> <span class="nav-label">{{ __('Logout') }}</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                            </form>
                        </li>
                    @endauth
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom mb-3">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i></a>
                        <ul class="nav navbar-top-links navbar-right">
                            <li style="padding: 20px">
                                <span class="m-r-sm text-muted">Bienvenido a {{ config('app.name') }}</span>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            @yield('content')

            <div class="footer">
                <div class="float-right">
                    <strong>{{ config('app.name') }}</strong>  &copy; {{ now()->year }}
                </div>
            </div>
        </div>
    </div>
    @toastr_render
</body>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    @yield('custom_scripts')
</html>


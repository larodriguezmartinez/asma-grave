@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox pb-4">
                <div class="ibox-title">
                    <h5>{{ __('Perfil') }}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row justify-content-center">
                            <div class="col-lg-8 col-md-12 ">
                                <div class="card">
                                    <div class="card-header">&nbsp;</div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('perfil.update' , Auth::user()->id ) }}">
                                            @method('PATCH')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                                <div class="col-md-6">
                                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                                                <div class="col-md-6">
                                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $user->surname }}" required autocomplete="surname" autofocus>

                                                    @error('surname')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email  }}" onlyread disabled>

                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="hospital" class="col-md-4 col-form-label text-md-right">{{ __('Hospital') }}</label>

                                                <div class="col-md-6">
                                                    <input id="hospital" type="hospital" class="form-control @error('hospital') is-invalid @enderror" name="hospital" value="{{ $user->hospital }}" required autocomplete="hospital">

                                                    @error('hospital')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label for="medical_speciality" class="col-md-4 col-form-label text-md-right">{{ __('Especialidad') }}</label>

                                                <div class="col-md-6">
                                                    <input id="medical_speciality" type="medical_speciality" class="form-control @error('medical_speciality') is-invalid @enderror" name="medical_speciality" value="{{ $user->medical_speciality }}"  required autocomplete="new-medical_speciality">

                                                    @error('medical_speciality')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="province" class="col-md-4 col-form-label text-md-right">{{ __('Provincia') }}</label>
                                                <div class="col-md-6">
                                                    <input id="province" type="province" class="form-control @error('province') is-invalid @enderror" name="province"  value="{{ $user->province }}"  required autocomplete="new-province">

                                                    @error('province')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary float-right">
                                                        {{ __('Actualizar') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="ibox-footer">
                    <span class="float-right">{{ __('Perfil') }}</span>{{ config('app.name') }}
                </div>
            </div>
        </div>
    </div>

@endsection

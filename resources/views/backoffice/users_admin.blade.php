@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ __('Alta nuevo usuario') }}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row justify-content-center">
                            <div class="col-12 ">
                                <div class="card">
                                    <div class="card-header">{{ __('Alta nuevo usuario') }}</div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('perfil.create') }}">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="name" class="col-md-2 col-form-label  text-md-right">{{ __('Nombre') }}</label>
                                                <div class="col-md-4">
                                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <label for="surname" class="col-md-2 col-form-label  text-md-right">{{ __('Apellidos') }}</label>
                                                <div class="col-md-4">
                                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>
                                                    @error('surname')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                                                <div class="col-md-4">
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hospital" class="col-md-2 col-form-label text-md-right">{{ __('Hospital') }}</label>
                                                <div class="col-md-4">
                                                    <input id="hospital" type="hospital" class="form-control @error('hospital') is-invalid @enderror" name="hospital" value="{{ old('hospital') }}"required autocomplete="hospital">
                                                    @error('hospital')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <label for="medical_speciality" class="col-md-2 col-form-label text-md-right">{{ __('Especialidad') }}</label>
                                                <div class="col-md-4">
                                                    <input id="medical_speciality" type="medical_speciality" class="form-control @error('medical_speciality') is-invalid @enderror" name="medical_speciality" value="{{ old('medical_speciality') }}"  required autocomplete="new-medical_speciality">
                                                    @error('medical_speciality')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="province" class="col-md-2 col-form-label text-md-right">{{ __('Provincia') }}</label>
                                                <div class="col-md-4">
                                                    <input id="province" type="province" class="form-control @error('province') is-invalid @enderror" name="province"  value="{{ old('province') }}"  required autocomplete="new-province">

                                                    @error('province')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password" class="col-md-2 col-form-label text-md-right">{{ __('Contraseña') }}</label>
                                                <div class="col-md-4">
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <label for="password-confirm" class="col-md-2 col-form-label text-md-right">{{ __('Confirmar contraseña') }}</label>
                                                <div class="col-md-4">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 col-form-label text-md-right">{{ __('Tipo usuario') }}</label>
                                                <div class="col-md-4">
                                                    <label class="pt-2 pl-3"><input required type="radio" value="1" name="rol" {{ old('rol') == 1 ? 'checked' : ''}} id="option-admin"> {{ __('Administrador') }} </label>
                                                    <label class="pt-2 pl-3"><input required type="radio" value="2" name="rol" {{ old('rol') == 2 ? 'checked' : ''}} id="option-user"> {{ __('Usuario') }} </label>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary float-right">
                                                        {{ __('Crear') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="ibox-footer">
                    <span class="float-right">{{ __('Gestión Usuarios') }}</span>{{ config('app.name') }}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
                <div class="ibox pb-5">
                    <div class="ibox-title">
                        <h5>{{ __('Gestión Usuarios') }}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row justify-content-center">
                                <div class="col-12 ">
                                    <div class="card">
                                        <div class="card-header">{{ __('Gestion de usuarios') }}</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Tipo usuario</th>
                                                            <th>Nombre</th>
                                                            <th>Apellidos</th>
                                                            <th>Email</th>
                                                            <th>Hospital</th>
                                                            <th>Especialidad</th>
                                                            <th>Provincia</th>
                                                            <th>Estado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($users as $user)
                                                            <tr>
                                                                <td>{{ $user->id }}</td>
                                                                <td>{{ $user->role_id }}</td>
                                                                <td>{{ $user->name }}</td>
                                                                <td>{{ $user->surname }}</td>
                                                                <td>{{ $user->email }}</td>
                                                                <td>{{ $user->hospital }}</td>
                                                                <td>{{ $user->medical_speciality }}</td>
                                                                <td>{{ $user->province }}</td>
                                                                <td>{{ $user->status }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="ibox-footer">
                        <span class="float-right">{{ __('Gestión Usuarios') }}</span>{{ config('app.name') }}
                    </div>
                </div>
            </div>
    </div>

    @endsection

    @section('custom_scripts')
        <script>
            $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 10,
                    responsive: true,
                });

            });

        </script>
    @endsection

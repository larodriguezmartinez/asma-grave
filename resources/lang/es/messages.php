<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email' => 'Email',
    'Password' => 'Contraseña',
    'remember' => 'Recordarme',
    'forgotYourPassword' => 'Olvidaste tu contraseña?',
    'login' => 'Acceso',
    'register' => 'Registro',
    'name' => 'Nombre',
    'surname' => 'Apellido',

];


<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alberto',
            'surname' => 'Rodriguez',
            'email' => 'admin@asma-gestion.test',
            'password' => bcrypt('Secret1234'),
            'role_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Nombre',
            'surname' => 'Apellido',
            'email' => 'usuario@asma-gestion.test',
            'password' => bcrypt('Secret1234'),
            'role_id' => 2,
        ]);
    }
}

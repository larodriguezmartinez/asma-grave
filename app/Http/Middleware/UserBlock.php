<?php

namespace App\Http\Middleware;

use Closure;

class UserBlock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->user() && auth()->user()->status === 0 )
        {
            auth()->logout();

            return redirect(route('login'))->with('message', 'El usuario esta bloqueado. Contacte con el administrador del sitio web.');

        }

        return $next($request);
    }
}

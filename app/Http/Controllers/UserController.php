<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Show list of all users
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $users = User::all();

        return view('backoffice.users_admin')->with([ 'users' => $users ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if($id != \Auth::user()->id)
        {

            return view('home');

        }

        $user = User::find($id);


        return view('backoffice.perfil.show')->with([ 'user' => $user ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);

        $validate = Validator::make($request->all(),[
            'name'=>'string|required',
            'surname'=>'string|required',
            'hospital'=>'string|required',
            'medical_speciality'=>'string|required',
            'province'=>'string|required',
            'password' => 'min:8|confirmed|nullable',
        ]);

        if($validate->fails())
        {
            return redirect()->back()->withInput()->withErrors($validate);
        }


        $user->name =  $request->name;
        $user->surname = $request->surname;
        $user->hospital = $request->hospital;
        $user->medical_speciality = $request->medical_speciality;
        $user->province = $request->province;

        if($request->password != '')
        {
            $user->password = bcrypt($request->password);
        }

        if($user->save())
        {
            toastr()->success('Usuario actualizado con éxito.');

        }else{

            toastr()->error('Ha ocurrido un error intentelo más tarde');

        }

        return view('backoffice.perfil.show')->with([ 'user' => $user ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function create(Request $request)
    {

        $user = new User;

        $validate = Validator::make($request->all(),[
            'name'=>'string|required',
            'surname'=>'string|required',
            'email' => 'email|required|unique:users',
            'hospital'=>'string|required',
            'medical_speciality'=>'string|required',
            'province'=>'string|required',
            'password' => 'min:8|confirmed|nullable',
            'rol' => 'numeric|required|between:1,2',
        ]);

        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }


        $user->name =  $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->hospital = $request->hospital;
        $user->medical_speciality = $request->medical_speciality;
        $user->province = $request->province;
        $user->password = bcrypt($request->password);
        $user->role_id = $request->rol;

        if($user->save())
        {
            toastr()->success('Usuario creado con éxito.');

        }else{

            toastr()->error('Ha ocurrido un error intentelo más tarde');

        }

        return redirect()->back();

    }

}

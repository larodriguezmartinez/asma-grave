<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\UserController;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);

Route::middleware(['auth'])->prefix('back-office')->group(function(){

    Route::get('/', function () {
        return view('home');
    })->name('home');

    // User profile routes
    Route::resource('perfil', 'UserController', ['only' => ['show', 'update']]);

    // User profile routes
    Route::get('registros', function () {
        return view('backoffice.record');
    })->name('registros');

    // Visit routes
    Route::get('visitas', function () {
        return view('backoffice.visit');
    })->name('visitas');

    // Transfer route
    Route::get('transferencias', function () {
        return view('backoffice.transfer');
    })->name('transferencias');

    // Admin User routes
    Route::middleware(['admin'])->group(function(){

        Route::get('panel-control', function () {
            return view('backoffice.panel');
        })->name('panel-control');

        Route::get('gestion-usuarios', 'UserController@index')->name('gestion-usuarios');

        Route::post('gestion-usuarios', 'UserController@create')->name('perfil.create');

    });

});
